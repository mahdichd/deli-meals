import 'package:flutter/material.dart';
import '../models/meal.dart';
import '../widgets/meal_item.dart';

class FavoritesScreen extends StatelessWidget {

  final List<Meal> favoriteMeals;
  FavoritesScreen(this.favoriteMeals);
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final isPortrait = mediaQuery.orientation == Orientation.portrait;
    final double itemHeight = (isPortrait) ? (mediaQuery.size.height -  - mediaQuery.padding.top) * 0.3
        : (mediaQuery.size.height  - mediaQuery.padding.top) * 0.6 ;

    final mealsList = ListView.builder(itemBuilder: (ctx,index){
      return MealItem(id: favoriteMeals[index].id,
          title: favoriteMeals[index].title,
          imageUrl: favoriteMeals[index].imageUrl,
          affordability: favoriteMeals[index].affordability,
          complexity: favoriteMeals[index].complexity,
          duration: favoriteMeals[index].duration,
          itemHeight : itemHeight
      );
    },
      itemCount: favoriteMeals.length,);
    return (favoriteMeals.isEmpty) ?
    Center(child:  Text('You have no favorites meals yet',style: Theme.of(context).textTheme.headline6,) )
          : SafeArea(child: mealsList);
  }
}
