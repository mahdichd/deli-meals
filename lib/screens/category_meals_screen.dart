import 'dart:io';
import '../dummy_data.dart';
import '../widgets/meal_item.dart';
import '../models/meal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class CategoryMealsScreen extends StatelessWidget {

  static final nameRoute = '/category-meals';

  final List<Meal> availableMeals ;

  CategoryMealsScreen(this.availableMeals);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final isPortrait = mediaQuery.orientation == Orientation.portrait;

    final routeArgs = ModalRoute.of(context).settings.arguments as Map<String,String> ;
    final categoryTitle = routeArgs['title'];
    final categoryId = routeArgs['id'];
    final categoryMeals = availableMeals.where((meal) {
      return meal.categories.contains(categoryId);
    }).toList();

    final PreferredSizeWidget appBar =
    (Platform.isIOS || Platform.isMacOS)
        ?
    CupertinoNavigationBar(
        middle: Text(categoryTitle),
      )
    :
    AppBar(title: Text(categoryTitle));

    final double itemHeight = (isPortrait) ? (mediaQuery.size.height - appBar.preferredSize.height - mediaQuery.padding.top) * 0.37
        : (mediaQuery.size.height - appBar.preferredSize.height - mediaQuery.padding.top) * 0.75 ;


    final mealsList = ListView.builder(itemBuilder: (ctx,index){
      return MealItem(id: categoryMeals[index].id,
        title: categoryMeals[index].title,
        imageUrl: categoryMeals[index].imageUrl,
        affordability: categoryMeals[index].affordability,
        complexity: categoryMeals[index].complexity,
        duration: categoryMeals[index].duration,
          itemHeight : itemHeight
      );
    },
      itemCount: categoryMeals.length,);
    return
    (Platform.isIOS || Platform.isMacOS)
        ?
    CupertinoPageScaffold(
            navigationBar: appBar,
            child: SafeArea(
              child: mealsList ,
              ),
            )
        :
    Scaffold(
            appBar: appBar,
            body:  mealsList,

          );
  }
}
