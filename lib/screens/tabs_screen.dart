import 'dart:io';

import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './favorites_screen.dart';
import '../widgets/main_drawer.dart';
import '../models/meal.dart';

import './categories_screen.dart';

class TabsScreen extends StatefulWidget {

  final List<Meal> favoriteMeals;
  TabsScreen(this.favoriteMeals);
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  bool initialised;
  var tabLabel = 'Categories';
  final CupertinoTabController _controller =
  (Platform.isIOS || Platform.isMacOS)
      ? CupertinoTabController()
      :
  null;

  int _selectedPageIndex = 0;

  final tabLabels = ['Categories','Favorites'];

  void _selectPage(int index) {
    setState(() {
      //if(index == 1) tabLabel = 'Favorites';
      _selectedPageIndex = index;
    });
  }


  Widget _returnSelectedPage(int index) {
    switch (index) {
      case 0 :
        return CategoriesScreen();
      case 1 :
        {
        return FavoritesScreen(widget.favoriteMeals);
        }
      default :
        return CategoriesScreen();
    }
  }
@override
  void initState() {
  initialised = false;
  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<BottomNavigationBarItem> navigationBarItems = [
      BottomNavigationBarItem(
        icon: Icon(
          Icons.category,
        ),
        label: 'Categories',
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.star,
        ),
        label: 'Favorites',
      )
    ];


    Widget _buildTabScaffold =
    (Platform.isIOS || Platform.isMacOS) ?
    InnerDrawer(
        key: _innerDrawerKey,
        onTapClose: true, // default false
        swipe: true, // default true
        //colorTransitionChild: Color.red, // default Color.black54
        //colorTransitionScaffold: Color.black54, // default Color.black54

        //When setting the vertical offset, be sure to use only top or bottom
        offset: IDOffset.horizontal( 0.5 ),

        //scale: IDOffset.horizontal( 0.8 ), // set the offset in both directions

       // proportionalChildArea : true, // default true
        //borderRadius: 50, // default 0
        leftAnimationType: InnerDrawerAnimation.quadratic, // default static
       // backgroundDecoration: BoxDecoration(color: Colors.red ), // default  Theme.of(context).backgroundColor


        leftChild: Container(child : MainDrawer()), // required if rightChild is not set
        //rightChild: Container(), // required if leftChild is not set
        //  A Scaffold is generally used but you are free to use other widgets
        // Note: use "automaticallyImplyLeading: false" if you do not personalize "leading" of Bar
        scaffold:


    CupertinoPageScaffold(

      navigationBar: CupertinoNavigationBar(
        middle: Text('DeliMeals'),
              leading: GestureDetector(
                  onTap: () => _toggle() ,
                  child: Icon(Icons.menu)) ,
      ),
      child: CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: navigationBarItems,
        currentIndex: _selectedPageIndex,

      ),
      controller: _controller,
      tabBuilder: (BuildContext context, int index) {
//        if(initialised) _selectPage(index);
//        initialised = true;
        return _returnSelectedPage(index);
      },
    ))
    )

        :
    Scaffold(
        appBar: AppBar(title: Text(tabLabels[_selectedPageIndex])),
//        appBar: AppBar(
//          title: Text('DeliMeal'),
//          bottom: TabBar(
//            tabs: tabItems,
//          ),
//        ),

        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color.fromRGBO(255, 255, 255, 0.5),
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey,
          currentIndex: _selectedPageIndex,
          items: navigationBarItems,
          onTap: _selectPage,
        ),
        body: _returnSelectedPage(_selectedPageIndex),
      drawer: Drawer(child : MainDrawer()),
    );

    return _buildTabScaffold;

  }
  final GlobalKey<InnerDrawerState> _innerDrawerKey = GlobalKey<InnerDrawerState>();

  void _toggle()
  {
    _innerDrawerKey.currentState.toggle(
      // direction is optional
      // if not set, the last direction will be used
      //InnerDrawerDirection.start OR InnerDrawerDirection.end
        direction: InnerDrawerDirection.start
    );
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
