import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class FiltresScreen extends StatefulWidget {
  final Function setFiltres;
  Map<String,bool> currentFiltres ;
  static final nameRoute = '/filtres-screen';

  FiltresScreen(this.currentFiltres,this.setFiltres);

  @override
  _FiltresScreenState createState() => _FiltresScreenState();
}


class _FiltresScreenState extends State<FiltresScreen> {

  var _glutenFree = false;
  var _lactoseFree = false;
  var _isVegetarian = false;
  var _isVegan = false;
  @override
  void initState() {
     _glutenFree = widget.currentFiltres['gluten'];
     _lactoseFree = widget.currentFiltres['lactose'];
     _isVegetarian = widget.currentFiltres['vegetarian'];
     _isVegan = widget.currentFiltres['vegan'];
    super.initState();
  }


  void setSelectedFiltres(){
  final selectedFiltres = {
  'gluten' : _glutenFree,
  'lactose' : _lactoseFree,
  'vegetarian' : _isVegetarian,
  'vegan' : _isVegan,
  };
  widget.setFiltres(selectedFiltres);
  }
  @override
  Widget build(BuildContext context) {
    Widget _buildSwitchListTile(String title, String description, bool variable, Function function) {
      return Material(
        child: SwitchListTile(title: Text(title),value: variable,subtitle: Text(description),
          onChanged: function )
      );
    }
    final filtresContent = SafeArea(child :Column(
      children: [
        Container(
          padding: EdgeInsets.all(25),
          child: Text('Adjust your meal selection',style: Theme.of(context).textTheme.headline6),
        ),
        Expanded(
          child: ListView(
            children: [
              _buildSwitchListTile('Gluten-Free','Only include gluten free meals.',_glutenFree,(newVal){setState(() {
                _glutenFree = newVal;
              });}),
              _buildSwitchListTile('Lactose-Free','Only include lactose free meals.',_lactoseFree,(newVal){setState(() {
                _lactoseFree = newVal;
              });}),
              _buildSwitchListTile('Vegetarian','Only include vegetarian free meals.',_isVegetarian,(newVal){setState(() {
                _isVegetarian = newVal;
              });}),
              _buildSwitchListTile('Vegan','Only include vegan free meals.',_isVegan,(newVal){setState(() {
                _isVegan = newVal;
              });}),
            ],
          ),
        )
      ],
    )
    );
    return
      (Platform.isIOS || Platform.isMacOS)?
    CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Filtres"),
        trailing: GestureDetector(
            onTap: setSelectedFiltres,
            child: Icon(Icons.save_outlined)),
      ),
      child: filtresContent,
    )
    :
    Scaffold(
      appBar: AppBar( title: Text('Filtres'),actions: [IconButton(icon: Icon(Icons.save_outlined),
        onPressed: setSelectedFiltres,)],),
      body: filtresContent,
    );
  }
}
