import 'dart:io';
import 'package:deli_meals/dummy_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class MealDetailsScreen extends StatelessWidget {
  static final String nameRoute = '/meal-details-screen';
  final Function toggleFavorite;
  final Function isFavorite;

  MealDetailsScreen(this.toggleFavorite,this.isFavorite);



  @override
  Widget build(BuildContext context) {
    final mealId = ModalRoute.of(context).settings.arguments as String;
    final meal = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);
    final mediaQuery = MediaQuery.of(context);
    Widget favoriteIcon(String mealId){
      return isFavorite(mealId) ? Icon(Icons.star) : Icon(Icons.star_border);
    }
    final PreferredSizeWidget appBar =
    (Platform.isIOS || Platform.isMacOS)
        ?
        CupertinoNavigationBar(
            middle: Text(meal.title),
            trailing: GestureDetector(child :  favoriteIcon(mealId),
                onTap: () => toggleFavorite(mealId),),
          )
        :
    AppBar(title: FittedBox(fit: BoxFit.fitWidth, child: Text(meal.title)));
    final isPortrait = mediaQuery.orientation == Orientation.portrait;
    final screenSize = mediaQuery.size.height -
        appBar.preferredSize.height -
        mediaQuery.padding.top;
    Widget _buildIngredientsContent() {
      return Container(
          //constraints: BoxConstraints(minHeight:20 ,maxHeight: screenSize * 0.30),
          //height: null,
          height: (meal.ingredients.length < 5)
              ? null
              : (mediaQuery.size.height < 550)
                  ? screenSize * 0.33
                  : screenSize * 0.25,
          margin: EdgeInsets.only(bottom: 10, left: 10, right: 10, top: 1),
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 3),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15)),
          child: ListView.builder(
              itemBuilder: (ctx, index) => Container(
                    //height: screenSize*0.045,
                    child: Card(
                      child: Padding(
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                          child: Text(meal.ingredients[index])),
                    ),
                  ),
              itemCount: meal.ingredients.length,
              shrinkWrap: true)
      );
    }
      Widget _buildStepsContent() {
        return Container(
          // constraints: BoxConstraints(minHeight:20 ,maxHeight: screenSize * 0.66),
          //height: screenSize * 0.33,
            height: (meal.steps.length < 6)
                ? null
                : (mediaQuery.size.height < 550)
                ? screenSize * 0.77
                : screenSize * 0.55,
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 3),
            margin: EdgeInsets.only(bottom: 10, left: 10, right: 10, top: 1),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(15)),
            child: ListView.builder(
                itemBuilder: (ctx, index) => Material(
                  child: Column(
                    children: [
                      Container(
                        //height: screenSize*0.07 ,
                        child: ListTile(
                          leading: CircleAvatar(
                            child: Text(
                              '#${index + 1}',
                            ),
                          ),
                          title: Text(meal.steps[index]),
                        ),
                      ),
                      Divider()
                    ],
                  ),
                ),
                itemCount: meal.steps.length,
                shrinkWrap: true));
      }



    final detailsContent = SingleChildScrollView(
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              height: (isPortrait) ? screenSize * 0.4 :  screenSize * 0.55,
              width: double.infinity,
              child: Image.network(
                meal.imageUrl,
                fit: BoxFit.cover,
              )),
          Container(
            height: (isPortrait) ? screenSize * 0.07 : screenSize * 0.1,
            //margin: EdgeInsets.all(10),
            child: Center(
                child: Text(
              'Ingredients',
              style: Theme.of(context).textTheme.headline6,
            )),
          ),
          _buildIngredientsContent(),
          Container(
            height: screenSize * 0.07,
            //margin: EdgeInsets.all(10),
            child: Center(
                child: Text(
              'Steps',
              style: Theme.of(context).textTheme.headline6,
            )),
          ),
          _buildStepsContent()
        ],
      ),
    );

    return
      (Platform.isIOS || Platform.isMacOS)
        ? CupertinoPageScaffold(
            navigationBar: appBar,
            child: SafeArea(
              child: detailsContent,
            ),
          )
        :
      Scaffold(
            appBar: appBar,
            body: detailsContent,
            floatingActionButton: FloatingActionButton(
              child: favoriteIcon(mealId),
              onPressed: () => toggleFavorite(mealId),
            ) ,
          );
  }
}
