import 'dart:io';

//import './category_meals_screen.dart';
import '../widgets/category_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../dummy_data.dart';

class CategoriesScreen extends StatelessWidget {

  static final String nameRoute = '/';

  @override
  Widget build(BuildContext context) {
    final categorieContent = SafeArea(
        child: GridView(
          padding: EdgeInsets.all(25),
          children: DUMMY_CATEGORIES
              .map(
                (catData) => CategoryItem(catData.id,catData.title, catData.color),
          )
              .toList(),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
        )
    );
    return
      (Platform.isIOS || Platform.isMacOS)
        ?
      CupertinoPageScaffold(
//            navigationBar:
//            CupertinoNavigationBar(
//              middle: Text('Categories'),
////              leading: GestureDetector(
////                  onTap: () => null ,
////                  child: Icon(CupertinoIcons.add)) ,
//            ),
            child: categorieContent,


          )
        :
      Scaffold(
           // appBar: AppBar(title: Text('Categories')),
            body: categorieContent,

          );
  }
}
