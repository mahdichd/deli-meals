import 'dart:ui';

import 'package:deli_meals/dummy_data.dart';

import './screens/filtresScreen.dart';
import './screens/tabs_screen.dart';
import './screens/meal_details_screen.dart';
import './screens/category_meals_screen.dart';
import './models/meal.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  Map<String,bool> _filtres = {
    'gluten' : false,
    'lactose' : false,
    'vegetarian' : false,
    'vegan' : false,
  };

  List<Meal> favoriteMeals =[];
  List<Meal> _availableMeals = DUMMY_MEALS;
  void _setFiltres(Map<String,bool> selectedFiltres){
    setState(() {
      print('selected filtres :');
      print(selectedFiltres);
      _filtres = selectedFiltres;
      print(' filtres :');
      print(_filtres);
      _availableMeals =
          DUMMY_MEALS.where((meal) {
            if(_filtres['gluten'] && !meal.isGlutenFree ) return false;
            if(_filtres['lactose'] && !meal.isLactoseFree ) return false;
            if(_filtres['vegetarian'] && !meal.isVegetarian ) return false;
            if(_filtres['vegan'] && !meal.isVegan ) return false;
            else return true;
          }).toList();
    });
  }

  void _toggleFavorite(String mealId){
    setState(() {
      (favoriteMeals.indexWhere((meal) => meal.id == mealId) < 0) ?
      favoriteMeals.add(DUMMY_MEALS.firstWhere((meal) => meal.id == mealId))
          : favoriteMeals.removeWhere((meal) => meal.id == mealId);
    });
  }

  bool _isFavorite(String mealId){
    return favoriteMeals.any((meal) => meal.id == mealId);
  }
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'DeliMeals',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Colors.purple,
        primaryColor: Color.fromRGBO(52, 152, 219, 1),
        canvasColor: Color.fromRGBO(255, 255, 255, 1),
        fontFamily: 'NoticaText',
        textTheme: ThemeData.light().textTheme.copyWith(
          headline6: TextStyle(
            fontSize: 20,
            fontFamily: 'Acme'
          )
        )
      ),
      home: TabsScreen(favoriteMeals),
      routes: {
        CategoryMealsScreen.nameRoute : (ctx) => CategoryMealsScreen(_availableMeals),
        MealDetailsScreen.nameRoute : (ctx) => MealDetailsScreen(_toggleFavorite,_isFavorite),
        FiltresScreen.nameRoute : (ctx) => FiltresScreen(_filtres,_setFiltres),
      },
    );
  }
}

