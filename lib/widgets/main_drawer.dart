
import 'package:flutter/material.dart';
import '../screens/filtresScreen.dart';

class MainDrawer extends StatelessWidget {

  Widget _buildListTile({String title, IconData icon, Function handlerFct, BuildContext context}){
    return Material(child: ListTile(
      title: Text(title,style: TextStyle(fontFamily: 'NoticiaText',fontSize: 19 , fontWeight: FontWeight.bold),),
      leading: Icon(icon,size: 25,),
      onTap: () => handlerFct()

    ));
  }


  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Column(
          children: [
            Container(
              height: mediaQuery.size.height * 0.23,
              color: Theme.of(context).accentColor,
              child: Center(
                child: Text('Cooking Up!',style: Theme.of(context).textTheme.headline6,),
              ),
            ),
            SizedBox(
              height: mediaQuery.size.height * 0.025,
            ),
            _buildListTile(
              title: 'Meals',
              icon : Icons.restaurant,
            handlerFct: (){Navigator.of(context).popAndPushNamed("/");},
            context: context),
            SizedBox(
              height: mediaQuery.size.height * 0.015,),
            _buildListTile(
                title: 'Filters',
                icon : Icons.settings,
            handlerFct: (){Navigator.of(context).popAndPushNamed(FiltresScreen.nameRoute);},
            context: context),
          ],
        ),

    );
  }
}
