import 'package:deli_meals/models/meal.dart';
import 'package:deli_meals/screens/meal_details_screen.dart';
import 'package:flutter/material.dart';

class MealItem extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;
  final int duration;
  final double itemHeight;
  final Complexity complexity;
  final Affordability affordability;

  MealItem({this.id,this.title,this.imageUrl,this.duration,this.complexity,this.affordability,this.itemHeight});

  String get complexityText{
    switch(complexity) {
      case Complexity.Simple:
        return 'Simple';
        break;
      case Complexity.Challenging:
        return 'Challenging';
        break;
      case Complexity.Hard:
        return 'Hard';
        break;
      default:
        return 'Unknown';
    }
  }

  String get affordabilityText{
    switch(affordability) {
      case Affordability.Affordable:
        return 'Affordable';
        break;
      case Affordability.Pricey:
        return 'Pricey';
        break;
      case Affordability.Luxurious:
        return 'Expensive';
        break;
      default:
        return 'Unknown';
    }
  }
  void selectMeal(BuildContext context){
    Navigator.of(context).pushNamed(MealDetailsScreen.nameRoute,arguments:id);
  }

  @override
  Widget build(BuildContext context) {


    return Material(
      child: InkWell(
        onTap: () => selectMeal(context),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15)
          ),
          elevation: 4,
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              Stack(
                children :[ ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)
                  ),
                  child: Image.network(imageUrl,height: itemHeight * 0.8, width: double.infinity,fit: BoxFit.cover,),
                ),
                  Positioned(
                    child: Container(
                      color: Colors.black45,
                      child: Center(child: Text(title,style: TextStyle(color: Colors.white, fontFamily: 'Acme',  fontSize: 19,),softWrap: true,overflow: TextOverflow.fade,)),
                    ),
                    bottom: 10,
                    right: 20,
                    left: 20,
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: [
                        Icon(Icons.schedule),
                        SizedBox(width: 6,),
                        Text('$duration min')
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.work),
                        SizedBox(width: 6,),
                        Text(complexityText)
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.attach_money),
                        SizedBox(width: 6,),
                        Text(affordabilityText)
                      ],
                    )
                  ],
                ),
              )
              
            ],
          ),
        ),
      ),
    );
  }
}
